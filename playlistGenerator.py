#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 15:34:34 2017

@author: Alexis DOMINIQUE
"""
# Importations des différents modules
import psycopg2
import argparse
import random

# connection à la base de données
connect_str = "dbname='adominique' user='a.dominique' host='postgresql.bts-malraux72.net' password='examenprojetmdp'"
# esaie de connection à la base de donnée
try:
    conn = psycopg2.connect(connect_str)
except Exception as error:
    print("Impossible de faire une connection à la base de données")
    exit(1)
# création d'un curseur requête psycopg2
cursor = conn.cursor()

# Gestion des arguments de la Générateur de Playlist
arguments = argparse.ArgumentParser(description='Générateur de playlist')
# création d'un argument de la durée de la playlist
arguments.add_argument("--duree", type=int, help="durée de la playlist", required=True)
arguments.add_argument("--genre", help="genre de la playlist", action="store")
arguments.add_argument("--artiste", help="artiste de la playlist", action="store")
arguments.add_argument("--titre", help="titre d'une musique", action="store")
arguments.add_argument("--marge", help="marge minimum de temps de blanc", action="store", default=30, type=int)
args = arguments.parse_args()

# Mise des minutes en secondes
tempsEnSecondeDuree = int(args.duree)*60;

# Initialisation
somme_durees = 0 # temps en seconde de toutes les musiques
duree_morceau = 0
cpt_tuples = 0 # incrémentation de la liste
pistes = [] # liste de tuples
suffix = "_tab" # suffixe des listes arguments

if(args.genre) is not None or (args.artiste) is not None or (args.titre) is not None:
    # Création des listes : genre, artiste et titre
    for VARIANT in ['genre', 'artiste', 'titre']:
        if getattr(args, VARIANT) is not None:
            try:
                cursor.execute(
                        "SELECT titre, album, artiste, genre, duree, chemin FROM morceaux WHERE %s ~'%s' ORDER BY RANDOM()" 
                        % (VARIANT, getattr(args, VARIANT))
                )
                # fetchall retourne une liste de tuples : [(,,), (,,), (,,)]
                setattr(args, VARIANT+suffix, cursor.fetchall())
                # Ajouts des pistes différentes pistes dans une seule variable
                pistes.extend(getattr(args, VARIANT+suffix))
            except Exception as e:   
                print("Impossible de faire quelque chose")
                exit(1)
else:
    # Choisir des pistes aléatoirement pour compléter la durée totale
    try:
        cursor.execute(
                "SELECT titre, album, artiste, genre, duree, chemin FROM morceaux ORDER BY RANDOM()" 
        )
        # Ajouts des pistes différentes pistes dans une seule variable
        pistes.extend(cursor.fetchall())
    except Exception as e:   
        print("Impossible de faire quelque chose conçernant le paramètre \"durée\"")
        exit(1)

# Mélange aléatoire des éléments de la liste pistes   
random.shuffle(pistes)

element_selectionne = False

# Boucle tant que la sommes des durées des morceaux est inférieur ou égal au temps désiré
while somme_durees <= tempsEnSecondeDuree - args.marge:
    duree_morceau = pistes[cpt_tuples][4]
    if(somme_durees + duree_morceau <= tempsEnSecondeDuree):
        somme_durees = somme_durees + pistes[cpt_tuples][4]
        element_selectionne = True
        # Affichage des pistes récupéré (piste)
        affichage = "{artiste} - {titre} - {duree}  seconde(s), de genre : {genre}".format(
                artiste=pistes[cpt_tuples][2],
                titre=pistes[cpt_tuples][0],
                duree=pistes[cpt_tuples][4],
                genre=pistes[cpt_tuples][3])
        print(affichage)
    cpt_tuples = (cpt_tuples + 1) % len(pistes)
    if(cpt_tuples % len(pistes) == 0):
        if(element_selectionne == True):
            element_selectionne = False
        else:
            break

# Calcul du temps en seconde de blanc
tempsEnSecondeAleatoire = tempsEnSecondeDuree-somme_durees
print("Il reste " + str(tempsEnSecondeAleatoire) + " seconde(s) restantes")
